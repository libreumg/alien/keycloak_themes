#!/bin/bash
apt-get update -y ; apt-get install -y fakeroot 
V=`more keycloak-themes/DEBIAN/control | grep "Version" | sed -e "s/Version: //g"`


chmod 755 keycloak-themes/DEBIAN
chmod 644 keycloak-themes/DEBIAN/*
#chmod 755 keycloak-themes/DEBIAN/postinst

echo "........................................................."

fakeroot dpkg -b keycloak-themes keycloak-themes_$V.deb
